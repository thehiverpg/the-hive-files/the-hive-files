# The Hive Files

Plugin para criação de ficha do modulo GURPS

### Instalar pacotes

> npm install or yarn install

### Compilar arquivos de JS e CSS

> grunt watch

### Minificar arquivos de JS e CSS

> grunt deploy

### Criar arquivo de tradução .pot

> grunt makepot
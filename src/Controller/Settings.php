<?php
namespace TH\Controller;

if ( ! function_exists( 'add_action' ) ) {
	exit( 0 );
}

use Carbon_Fields\Container;
use Carbon_Fields\Field;

use TH\Core;

class Settings
{
	public function __construct()
	{   
        $this->basename = Core::plugin_basename();
        add_filter( 'carbon_fields_register_fields', array( $this, 'ac_plugin_options' ) );
    }
    
    public function ac_plugin_options()
	{
        Container::make( 'theme_options', 'America Chip' )
        ->set_page_parent( 'options-general.php' )
        ->add_tab(
            __( 'Meliuz', Core::TEXTDOMAIN ),
            array(
                Field::make( 'text', 'ac_meliuz_url', __( 'MELIUZ Url', Core::TEXTDOMAIN ) )
                ->set_default_value( 'https://www.meliuz.com.br/pixel/pa/6356.png' )
                ->set_attribute( 'placeholder', 'https://www.meliuz.com.br/pixel/pa/6356.png' )
                ->set_help_text( __( 'Add Meliuz url', Core::TEXTDOMAIN ) ),
                Field::make( 'text', 'ac_auth_field', __( 'AUTH Value', Core::TEXTDOMAIN ) )
                ->set_width(50)
                ->set_default_value( '5bec4ecc04969' )
                ->set_attribute( 'placeholder', '5bec4ecc04969' )
                ->set_help_text( __( 'Add auth value', Core::TEXTDOMAIN ) ),
                Field::make( 'text', 'ac_comission_field', __( 'Comission Value', Core::TEXTDOMAIN ) )
                ->set_width(50)
                ->set_attribute( 'placeholder', '0.10' )
                ->set_help_text( __( 'Add comission value', Core::TEXTDOMAIN ) ),
            )
        )
        ->add_tab(
            __( 'Logs', Core::TEXTDOMAIN ),
            array(
                Field::make( 'text', 'ac_log_file_name', __( 'Log File Name', Core::TEXTDOMAIN ) )
                ->set_width(50)
                ->set_default_value( 'america-chip' )
                ->set_attribute( 'placeholder', 'america-chip' )
                ->set_help_text( __( 'Add log file name, example: log-name or log_name', Core::TEXTDOMAIN ) ),
                Field::make( 'checkbox', 'ac_log_field', __( 'Enable Log', Core::TEXTDOMAIN ) )
                ->set_width(50)
                ->set_help_text( __( 'Create order log. WooCommerce > Status > Logs', Core::TEXTDOMAIN ) ),
            )
        );
    }
}
<?php
namespace TH\Controller\PostTypes;

if ( ! function_exists( 'add_action' ) ) {
	exit( 0 );
}

use Carbon_Fields\Container;
use Carbon_Fields\Field;

use TH\Core;
use TH\Helper\Utils;
use TH\Helper\Queries;

class Adventures
{
	public function __construct()
	{
        $this->post_type = 'the-hive-adventures';
        $this->queries   = new Queries();

        add_action( 'init', array( $this, 'adventures_post_type' ) );
        add_filter( 'carbon_fields_register_fields', array( $this, 'adventures_options' ) );
        add_filter( 'enter_title_here', array( $this, 'adventures_title' ), 10, 2 );
        add_filter( 'manage_' . $this->post_type . '_posts_columns' , array( $this, 'adventures_name_columns' ) );
		add_action( 'manage_' . $this->post_type . '_posts_custom_column', array( $this, 'adventures_columns_content' ), 10, 2 );
        add_action( 'save_post', array( $this, 'adventures_update_character' ), 20, 2 );
    }

    public function adventures_title( $title, $post )
    {
        if ( $post->post_type === $this->post_type ) {
            return __( 'Enter your adventure name here', Core::TEXTDOMAIN );
        }

        return $title;
    }

    public function adventures_post_type()
    {
        $labels = array(
            'name'           => __( 'Adventures', Core::TEXTDOMAIN ),
            'singular_name'  => __( 'Adventure', Core::TEXTDOMAIN ),
            'add_new'        => __( 'Add adventure', Core::TEXTDOMAIN ),
            'add_new_item'   => __( 'Add New adventure', Core::TEXTDOMAIN ),
            'new_item'       => __( 'New adventure', Core::TEXTDOMAIN ),
            'edit_item'      => __( 'Edit adventure', Core::TEXTDOMAIN ),
            'view_item'      => __( 'View adventures', Core::TEXTDOMAIN ),
            'all_items'      => __( 'All adventures', Core::TEXTDOMAIN ),
            'search_items'   => __( 'Search adventure', Core::TEXTDOMAIN ),
            'not_found'      => __( 'No adventure found.', Core::TEXTDOMAIN ),
            );

        register_post_type(
            $this->post_type,
            array(
                'labels'  => $labels,
                'public'      => true,
                'has_archive' => true,
                'menu_icon'   => 'dashicons-clipboard',
                'rewrite'     => array( 'slug' => 'adventures' ),
                'supports'    => array( 'title', 'thumbnail' ),
            )
        );
    }

    public function adventures_options()
	{
        //var_dump($this->queries::get_character_author());die;
        Container::make( 'post_meta', __( 'Adventure Creation', Core::TEXTDOMAIN ) )
        ->where( 'post_type', '=', $this->post_type )
        ->add_fields( array(
            Field::make( 'text', 'adventure_points', __( 'Adventure Points', Core::TEXTDOMAIN ) )
            ->set_attribute( 'type', 'number' )
            ->set_width( 20 ),
            Field::make( 'text', 'adventure_disadvantage', __( 'Disadvantage Number', Core::TEXTDOMAIN ) )
            ->set_attribute( 'type', 'number' )
            ->set_width( 20 ),
            Field::make( 'date', 'adventure_date_creation', __( 'Date Creation', Core::TEXTDOMAIN ) )
            ->set_input_format( 'd/m/Y', 'd/m/Y' )
            ->set_storage_format( 'Y-m-d' )
            ->set_width( 60 )
        ) );

        Container::make( 'post_meta', __( 'Adventure Story', Core::TEXTDOMAIN ) )
        ->where( 'post_type', '=', $this->post_type )
        ->add_fields( array(
            Field::make( 'rich_text', 'adventure_story', '' )
        ) );

        $labels = array(
            'plural_name'   => __( 'Players', Core::TEXTDOMAIN ),
            'singular_name' => __( 'Player', Core::TEXTDOMAIN )
        );

        Container::make( 'post_meta', __( 'Players', Core::TEXTDOMAIN ) )
        ->where( 'post_type', '=', $this->post_type )
        ->add_fields( array(
            Field::make( 'complex', 'adventure_players', '' )
            ->set_layout( 'tabbed-horizontal' )
            ->setup_labels( $labels )
            ->add_fields( array(
                Field::make( 'select', 'adventure_players_select', __( 'Select an Player', Core::TEXTDOMAIN ) )
                ->add_options( array( $this->queries, 'get_character_author' ) )
                ->set_width( 50 ),
                Field::make( 'text', 'adventure_player_xp', __( 'XP', Core::TEXTDOMAIN ) )
                ->set_attribute( 'type', 'number' )
                ->set_width( 30 ),
                Field::make( 'text', 'adventure_player_level', __( 'Level', Core::TEXTDOMAIN ) )
                ->set_attribute( 'type', 'number' )
                ->set_width( 20 )
            ) )
        ) );
    }

    public function adventures_name_columns( $columns )
	{
		$new_columns = array(
            'title'             => __( 'Adventure Name', Core::TEXTDOMAIN ),
			'adventure_date'    => __( 'Adventure Date', Core::TEXTDOMAIN ),
            'adventure_points'  => __( 'Adventure Points', Core::TEXTDOMAIN ),
            'adventure_players' => __( 'Adventure Players', Core::TEXTDOMAIN ),
			'date'              => $columns['date'],
		);

		unset( $columns['date'] );
		return array_merge( $columns, $new_columns );
	}

	public function adventures_columns_content( $column, $post_id )
	{
        $creation_date = get_post_meta( $post_id, '_adventure_date_creation', true );

		switch ( $column ) {
			case 'adventure_date' :
				$column_content = Utils::date_pt_format( $creation_date );
				break;
			case 'adventure_points' :
				$column_content = get_post_meta( $post_id, '_adventure_points', true );
                break;
            case 'adventure_players' :
                $column_content = get_post_meta( $post_id, '_adventures_player_number', true );
                break;
			default:
				# code...
				break;
		}

		echo ( ! empty( $column_content ) ) ? $column_content : '—';
    }

    public function adventures_update_character( $post_id, $post )
    {
        $this->queries::get_character_post( $post_id, $post );
    }
}

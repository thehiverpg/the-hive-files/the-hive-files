<?php
namespace TH\Controller\PostTypes;

if ( ! function_exists( 'add_action' ) ) {
	exit( 0 );
}

use Carbon_Fields\Container;
use Carbon_Fields\Field;

use TH\Core;
use TH\Helper\Utils;
use TH\Helper\Repositories;

class Advantages
{
	public function __construct()
	{
        $this->post_type    = 'the-hive-advantages';
        $this->repositories = new Repositories();

        add_action( 'init', array( $this, 'advantages_post_type' ) );
        add_filter( 'carbon_fields_register_fields', array( $this, 'advantages_options' ) );
        add_filter( 'enter_title_here', array( $this, 'advantages_title' ), 10, 2 );
    }

    public function advantages_title( $title, $post )
    {
        if ( $post->post_type === $this->post_type ) {
            return __( 'Enter your character name here', Core::TEXTDOMAIN );
        }

        return $title;
    }

    public function advantages_post_type()
    {
        $labels = array(
            'name'           => __( 'Advantage', Core::TEXTDOMAIN ),
            'add_new'        => __( 'Add Advantage', Core::TEXTDOMAIN ),
            'add_new_item'   => __( 'Add New Advantage', Core::TEXTDOMAIN ),
            'new_item'       => __( 'New Advantage', Core::TEXTDOMAIN ),
            'edit_item'      => __( 'Edit Advantage', Core::TEXTDOMAIN ),
            'view_item'      => __( 'View Advantage', Core::TEXTDOMAIN ),
            'all_items'      => __( 'All Advantages', Core::TEXTDOMAIN ),
            'search_items'   => __( 'Search Advantage', Core::TEXTDOMAIN ),
            'not_found'      => __( 'No advantages found.', Core::TEXTDOMAIN ),
            );

        register_post_type(
            $this->post_type,
            array(
                'labels'       => $labels,
                'public'       => true,
                'has_archive'  => true,
                'show_in_menu' => Core::CHARACTER_CPT,
                'rewrite'      => array( 'slug' => 'advantages' ),
                'supports'     => array( 'title', 'thumbnail' ),
            )
        );
    }

    public function advantages_options()
	{
        Container::make( 'post_meta', __( 'Attributes Points', Core::TEXTDOMAIN ) )
        ->where( 'post_type', '=', $this->post_type )
        ->set_priority( 'high' )
        ->set_context( 'side' )
        ->add_fields( array(
            Field::make( 'text', 'advantages_points_spent', __( 'Points spent', Core::TEXTDOMAIN ) )
            ->set_help_text( __( 'Points spent on the character', Core::TEXTDOMAIN ) )
            ->set_attribute( 'type', 'number' )
            ->set_attribute( 'readOnly', true ),
        ) );

        $labels = array(
            'plural_name'   => __( 'Advantages', Core::TEXTDOMAIN ),
            'singular_name' => __( 'Advantage', Core::TEXTDOMAIN )
        );

        Container::make( 'post_meta', __( 'Advantages', Core::TEXTDOMAIN ) )
        ->where( 'post_type', '=', $this->post_type )
        ->add_fields( array(
            Field::make( 'complex', 'advantages_fields', '' )
            ->set_layout( 'tabbed-horizontal' )
            ->setup_labels( $labels )
            ->add_fields( array(
                Field::make( 'select', 'advantages_fields_json', __( 'Select an Advantages', Core::TEXTDOMAIN ) )
                ->add_options( array( $this->repositories, 'get_advantages_json' ) )
                ->set_width( 80 ),
                Field::make( 'text', 'adventure_points', __( 'Points', Core::TEXTDOMAIN ) )
                ->set_attribute( 'type', 'number' )
                ->set_attribute( 'min', 2 )
                ->set_width( 20 )
            ) )
        ) );
    }
}

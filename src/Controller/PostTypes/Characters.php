<?php
namespace TH\Controller\PostTypes;

if ( ! function_exists( 'add_action' ) ) {
	exit( 0 );
}

use Carbon_Fields\Container;
use Carbon_Fields\Field;

use TH\Core;
use TH\Helper\Repositories;
use TH\Helper\Queries;
use TH\Helper\Utils;

class Characters
{
	public function __construct()
	{ 
        $this->post_type    = 'the-hive-character';
        $this->repositories = new Repositories();
        $this->queries      = new Queries();

        add_action( 'init', array( $this, 'character_post_type' ) );
        add_filter( 'carbon_fields_register_fields', array( $this, 'character_options' ) );
        add_filter( 'enter_title_here', array( $this, 'character_title' ), 10, 2 );
        add_filter( 'manage_' . $this->post_type . '_posts_columns' , array( $this, 'character_name_columns' ) );
		add_action( 'manage_' . $this->post_type . '_posts_custom_column', array( $this, 'character_columns_content' ), 10, 2 );
    }

    public function character_title( $title, $post )
    {
        if ( $post->post_type === $this->post_type ) {
            return __( 'Enter your character name here', Core::TEXTDOMAIN );
        }
    
        return $title;
    }

    public function character_post_type()
    {
        $labels = array(
            'name'           => __( 'Characters File', Core::TEXTDOMAIN ),
            'singular_name'  => __( 'Character File', Core::TEXTDOMAIN ),
            'add_new'        => __( 'Add Character', Core::TEXTDOMAIN ),
            'add_new_item'   => __( 'Add New Character', Core::TEXTDOMAIN ),
            'new_item'       => __( 'New Character', Core::TEXTDOMAIN ),
            'edit_item'      => __( 'Edit Character', Core::TEXTDOMAIN ),
            'view_item'      => __( 'View Character', Core::TEXTDOMAIN ),
            'all_items'      => __( 'All Characters', Core::TEXTDOMAIN ),
            'search_items'   => __( 'Search Character', Core::TEXTDOMAIN ),
            'not_found'      => __( 'No character found.', Core::TEXTDOMAIN ),
            );
            
        register_post_type(
            $this->post_type,
            array(
                'labels'  => $labels,
                'public'      => true,
                'has_archive' => true,
                'menu_icon'   => 'dashicons-id-alt',
                'rewrite'     => array( 'slug' => 'files' ),
                'supports'    => array( 'title', 'thumbnail', 'author' ),
            )
        );
    }
    
    public function character_options()
	{
        //var_dump(Queries::get_adventures_points());
        Container::make( 'post_meta', __( 'Adventure Fields', Core::TEXTDOMAIN ) )
        ->where( 'post_type', '=', $this->post_type )
        ->set_priority( 'high' )
        ->set_context( 'side' )
        ->add_fields( array(
            Field::make( 'text', 'character_points_spend', __( 'Points to spend', Core::TEXTDOMAIN ) )
            ->set_help_text( __( 'Adventure points to spend', Core::TEXTDOMAIN ) )
            ->set_attribute( 'type', 'number' )
            ->set_attribute( 'readOnly', true ),
            Field::make( 'text', 'character_points_spent', __( 'Points spent', Core::TEXTDOMAIN ) )
            ->set_help_text( __( 'Points spent on the character', Core::TEXTDOMAIN ) )
            ->set_attribute( 'type', 'number' )
            ->set_attribute( 'readOnly', true ),
            Field::make( 'text', 'character_level', __( 'Level', Core::TEXTDOMAIN ) )
            ->set_attribute( 'type', 'number' )
            ->set_attribute( 'readOnly', true ),
            Field::make( 'text', 'character_xptotal', __( 'XP Total', Core::TEXTDOMAIN ) )
            ->set_attribute( 'type', 'number' )
            ->set_attribute( 'readOnly', true ),
        ) );

        Container::make( 'post_meta', __( 'Character Fields', Core::TEXTDOMAIN ) )
        ->where( 'post_type', '=', $this->post_type )
        ->add_fields( array(
            Field::make( 'select', 'character_adventures_fields', __( 'Adventures', Core::TEXTDOMAIN ) )
            ->set_required( true )
            ->set_width( 35 )
            ->add_options( array( $this->queries, 'get_adventures_names' ) ),
            Field::make( 'select', 'character_attributes_fields', __( 'Attributes', Core::TEXTDOMAIN ) )
            ->set_required( true )
            ->set_width( 35 )
            ->add_options( array( $this->queries, 'get_post_attributes' ) ),
            Field::make( 'date', 'character_creation_date', __( 'Creation Date', Core::TEXTDOMAIN ) )
            ->set_input_format( 'd/m/Y', 'd/m/Y' )
            ->set_storage_format( 'Y-m-d' )
            ->set_width( 30 ),
            Field::make( 'select', 'character_advantages_fields', __( 'Advantages', Core::TEXTDOMAIN ) )
            ->set_required( true )
            ->add_options( array( $this->queries, 'get_post_advantages' ) ),
        ) );
    }

    public function character_name_columns( $columns )
	{
		$new_columns = array(
            'title'           => __( 'Character Name', Core::TEXTDOMAIN ),
			'character_date'  => __( 'Character Date', Core::TEXTDOMAIN ),
            'character_xp'    => __( 'Character XP', Core::TEXTDOMAIN ),
            'character_level' => __( 'Character Level', Core::TEXTDOMAIN ),
			'date'            => $columns['date'],
		);

		unset( $columns['date'] );
		return array_merge( $columns, $new_columns );
	}

	public function character_columns_content( $column, $post_id )
	{
        $creation_date = get_post_meta( $post_id, '_character_creation_date', true );

		switch ( $column ) {
			case 'character_date' :
				$column_content = Utils::date_pt_format( $creation_date );
				break;
			case 'character_xp' :
				$column_content = get_post_meta( $post_id, '_character_xptotal', true );
                break;
            case 'character_level' :
                $column_content = get_post_meta( $post_id, '_character_level', true );
                break;
			default:
				# code...
				break;
		}

		echo ( ! empty( $column_content ) ) ? $column_content : '—';
    }
}
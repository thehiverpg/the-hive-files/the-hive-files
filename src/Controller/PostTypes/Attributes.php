<?php
namespace TH\Controller\PostTypes;

if ( ! function_exists( 'add_action' ) ) {
	exit( 0 );
}

use Carbon_Fields\Container;
use Carbon_Fields\Field;

use TH\Core;
use TH\Helper\Queries;

class Attributes
{
	public function __construct()
	{ 
        $this->post_type = 'the-hive-attributes';
        $this->queries   = new Queries();

        add_action( 'init', array( $this, 'attributes_post_type' ) );
        add_filter( 'carbon_fields_register_fields', array( $this, 'attributes_options' ) );
        add_filter( 'enter_title_here', array( $this, 'attributes_title' ), 10, 2 );
        add_filter( 'manage_' . $this->post_type . '_posts_columns' , array( $this, 'attributes_name_columns' ) );
		add_action( 'manage_' . $this->post_type . '_posts_custom_column', array( $this, 'attributes_columns_content' ), 10, 2 );
        add_action( 'save_post', array( $this, 'attributes_update_points' ), 20, 2 );
    }

    public function attributes_title( $title, $post )
    {
        if ( $post->post_type === $this->post_type ) {
            return __( 'Enter your character name here', Core::TEXTDOMAIN );
        }
    
        return $title;
    }

    public function attributes_post_type()
    {
        $labels = array(
            'name'           => __( 'Attribute', Core::TEXTDOMAIN ),
            'add_new'        => __( 'Add Attributes', Core::TEXTDOMAIN ),
            'add_new_item'   => __( 'Add New Attributes', Core::TEXTDOMAIN ),
            'new_item'       => __( 'New Attributes', Core::TEXTDOMAIN ),
            'edit_item'      => __( 'Edit Attributes', Core::TEXTDOMAIN ),
            'view_item'      => __( 'View Attributes', Core::TEXTDOMAIN ),
            'all_items'      => __( 'All Attributes', Core::TEXTDOMAIN ),
            'search_items'   => __( 'Search Attributes', Core::TEXTDOMAIN ),
            'not_found'      => __( 'No attributes found.', Core::TEXTDOMAIN ),
            );
            
        register_post_type(
            $this->post_type,
            array(
                'labels'       => $labels,
                'public'       => true,
                'has_archive'  => true,
                'show_in_menu' => Core::CHARACTER_CPT,
                'menu_icon'    => 'dashicons-id-alt',
                'rewrite'      => array( 'slug' => 'attributes' ),
                'supports'     => array( 'title' ),
            )
        );
    }
    
    public function attributes_options()
	{
        Container::make( 'post_meta', __( 'Attributes Points', Core::TEXTDOMAIN ) )
        ->where( 'post_type', '=', $this->post_type )
        ->set_priority( 'high' )
        ->set_context( 'side' )
        ->add_fields( array(
            Field::make( 'text', 'attribute_points_spent', __( 'Points spent', Core::TEXTDOMAIN ) )
            ->set_help_text( __( 'Points spent on the character', Core::TEXTDOMAIN ) )
            ->set_attribute( 'type', 'number' )
            ->set_attribute( 'readOnly', true ),
        ) );

        Container::make( 'post_meta', __( 'Attributes', Core::TEXTDOMAIN ) )
        ->where( 'post_type', '=', $this->post_type )
        ->add_fields( array(
            Field::make( 'text', 'attribute_strength_level', __( 'ST ( Level )', Core::TEXTDOMAIN ) )
            ->set_attribute( 'min', 1 )
            ->set_width( 25 )
            ->set_help_text( __( "Your character's strength", Core::TEXTDOMAIN ) )
            ->set_attribute( 'type', 'number' ),
            Field::make( 'text', 'attribute_strength_points', __( 'Cost in points', Core::TEXTDOMAIN ) )
            ->set_width( 25 )
            ->set_help_text( __( "Your character's strength", Core::TEXTDOMAIN ) )
            ->set_attribute( 'type', 'number' ),
            Field::make( 'text', 'attribute_dexterity_level', __( 'DX ( Level )', Core::TEXTDOMAIN ) )
            ->set_attribute( 'min', 1 )
            ->set_width( 25 )
            ->set_help_text( __( "Your character's dexterity", Core::TEXTDOMAIN ) )
            ->set_attribute( 'type', 'number' ),
            Field::make( 'text', 'attribute_dexterity_points', __( 'Cost in points', Core::TEXTDOMAIN ) )
            ->set_width( 25 )
            ->set_help_text( __( "Your character's dexterity", Core::TEXTDOMAIN ) )
            ->set_attribute( 'type', 'number' ),
            Field::make( 'text', 'attribute_intelligence_level', __( 'IQ ( Level )', Core::TEXTDOMAIN ) )
            ->set_attribute( 'min', 1 )
            ->set_width( 25 )
            ->set_help_text( __( "Your character's intelligence", Core::TEXTDOMAIN ) )
            ->set_attribute( 'type', 'number' ),
            Field::make( 'text', 'attribute_intelligence_points', __( 'Cost in points', Core::TEXTDOMAIN ) )
            ->set_width( 25 )
            ->set_help_text( __( "Your character's intelligence", Core::TEXTDOMAIN ) )
            ->set_attribute( 'type', 'number' ),
            Field::make( 'text', 'attribute_vitality_level', __( 'HT ( Level )', Core::TEXTDOMAIN ) )
            ->set_attribute( 'min', 1 )
            ->set_width( 25 )
            ->set_help_text( __( "Your character's vitality", Core::TEXTDOMAIN ) )
            ->set_attribute( 'type', 'number' ),     
            Field::make( 'text', 'attribute_vitality_points', __( 'Cost in points', Core::TEXTDOMAIN ) )
            ->set_width( 25 )
            ->set_help_text( __( "Your character's vitality", Core::TEXTDOMAIN ) )
            ->set_attribute( 'type', 'number' ),
        ) );
    }

    public function attributes_name_columns( $columns )
	{
		$new_columns = array(
            'title'            => __( 'Character Name', Core::TEXTDOMAIN ),
			'attribute_st'     => __( 'ST', Core::TEXTDOMAIN ),
            'attribute_dx'     => __( 'DX', Core::TEXTDOMAIN ),
            'attribute_iq'     => __( 'IQ', Core::TEXTDOMAIN ),
            'attribute_ht'     => __( 'HT', Core::TEXTDOMAIN ),
            'attribute_points' => __( 'Points Spent', Core::TEXTDOMAIN ),
			'date'             => $columns['date'],
		);

		unset( $columns['date'] );
		return array_merge( $columns, $new_columns );
	}

	public function attributes_columns_content( $column, $post_id )
	{
		switch ( $column ) {
			case 'attribute_st' :
				$column_content = get_post_meta( $post_id, '_attribute_strength_level', true );
				break;
			case 'attribute_dx' :
				$column_content = get_post_meta( $post_id, '_attribute_dexterity_level', true );
                break;
            case 'attribute_iq' :
                $column_content = get_post_meta( $post_id, '_attribute_intelligence_level', true );
                break;
            case 'attribute_ht' :
                $column_content = get_post_meta( $post_id, '_attribute_vitality_level', true );
                break;
            case 'attribute_points' :
                $column_content = get_post_meta( $post_id, '_attribute_points_spent', true );
                break;
			default:
				# code...
				break;
		}

		echo ( ! empty( $column_content ) ) ? $column_content : '—';
    }

    public function attributes_update_points( $post_id, $post )
    {
        $this->queries::get_attribute_points( $post_id, $post );
    }
}
<?php
namespace TH\Helper;

if ( ! function_exists( 'add_action' ) ) {
	exit( 0 );
}

use TH\Core;

class Queries
{
    public static function get_adventures_names()
	{
        global $post;

        $results = [];

        $args = get_posts(
            array(
            'post_type'   => 'the-hive-adventures',
            'post_status' => 'publish',
            'numberposts' => -1,
            'order'       => 'ASC'
            )
        );

        if ( $args ) {
            foreach ( $args as $post ) {
                $post_id           = $post->ID;
                $results[0]        = __( 'Select an adventure...', Core::TEXTDOMAIN );
                $results[$post_id] = $post->post_title;
            }
        }

        return $results;
    }

    public static function get_character_author()
	{
        $results = [];
        $post_id = isset( $_GET['post'] ) ? $_GET['post'] : '';

        $args = get_posts(
            array(
            'post_type'        => 'the-hive-character',
            'post_status'      => 'publish',
            'numberposts'      => -1,
            'meta_key'         => '_character_adventures_fields',
            'meta_value'       => $post_id,
            'suppress_filters' => true,
            )
        );

        if ( $args ) {
            foreach ( $args as $post ) {
                $user_id           = $post->post_author;
                $post_id           = $post->ID;
                $user              = get_the_author_meta( 'display_name', $user_id);
                $results[0]        = __( 'Select an player...', Core::TEXTDOMAIN );
                $results[$post_id] = $user .' ('.$post->post_title.')';
            }
        }

        return $results;
    }

    public static function get_character_post( $post_id, $post )
	{
        $post_type = get_post_type( $post_id );

        if ( 'the-hive-adventures' != $post_type ){
            return;
        }

        $results       = [];       
        $player_number = 0;
        $points        = get_post_meta( $post_id, '_adventure_points', true );

        $args = get_posts(
            array(
            'post_type'        => 'the-hive-character',
            'post_status'      => 'publish',
            'numberposts'      => -1,
            'meta_key'         => '_character_adventures_fields',
            'meta_value'       => $post_id,
            'suppress_filters' => true,
            )
        );

        if ( $args ) {
            foreach ( $args as $key => $post ) {
                $player_key    = '_adventure_players|adventure_players_select|'.$key.'|0|value';
                $xp_key        = '_adventure_players|adventure_player_xp|'.$key.'|0|value';
                $level_key     = '_adventure_players|adventure_player_level|'.$key.'|0|value';
                $player_id     = get_post_meta( $post_id, $player_key, true );
                $xp_value      = get_post_meta( $post_id, $xp_key, true );
                $level_value   = get_post_meta( $post_id, $level_key, true );
                $player_number = $key + $player_number;
                update_post_meta( (int)$player_id, '_character_points_spend', $points );
                update_post_meta( $player_id, '_character_xptotal', $xp_value );
                update_post_meta( $player_id, '_character_level', $level_value );
            }
        }

        update_post_meta( $post_id, '_adventures_player_number', $player_number );
    }

    public static function get_attribute_points( $post_id, $post )
	{
        $post_type = get_post_type( $post_id );

        if ( 'the-hive-attributes' != $post_type ){
            return;
        }

        $st_points    = get_post_meta( $post_id, '_attribute_strength_points', true );
        $dx_points    = get_post_meta( $post_id, '_attribute_dexterity_points', true );
        $iq_points    = get_post_meta( $post_id, '_attribute_intelligence_points', true );
        $ht_points    = get_post_meta( $post_id, '_attribute_vitality_points', true );
        $total_points = $st_points + $dx_points + $iq_points + $ht_points;
        update_post_meta( $post_id, '_attribute_points_spent', $total_points );       
    }

    public static function get_post_attributes()
	{
        global $current_user;
        get_currentuserinfo();
        $results = [];

        $args = get_posts(
            array(
                'post_type'        => 'the-hive-attributes',
                'post_status'      => 'publish',
                'author'           =>  $current_user->ID,
                'numberposts'      => -1,
                'suppress_filters' => true,
            )
        );

        if ( $args ) {
            foreach ( $args as $post ) {
                $post_id           = $post->ID;
                $results[0]        = __( 'Select an attribute...', Core::TEXTDOMAIN );
                $results[$post_id] = $post->post_title;
            }
        }

        return $results;
    }

    public static function get_post_advantages()
	{
        global $current_user;
        get_currentuserinfo();
        $results = [];

        $args = get_posts(
            array(
                'post_type'        => 'the-hive-advantages',
                'post_status'      => 'publish',
                'author'           =>  $current_user->ID,
                'numberposts'      => -1,
                'suppress_filters' => true,
            )
        );

        if ( $args ) {
            foreach ( $args as $post ) {
                $post_id           = $post->ID;
                $results[0]        = __( 'Select an advantage...', Core::TEXTDOMAIN );
                $results[$post_id] = $post->post_title;
            }
        }

        return $results;
    }
}

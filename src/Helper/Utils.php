<?php
namespace TH\Helper;

if ( ! function_exists( 'add_action' ) ) {
	exit( 0 );
}

use TH\Core;

class Utils
{
	/**
	 * Sanitize value from custom method
	 *
	 * @since 1.0
	 * @param String $name
	 * @param Mixed $default
	 * @param String|Array $sanitize
	 * @return Mixed
	*/
	public static function request( $type, $name, $default, $sanitize = 'rm_tags' )
	{
		$request = filter_input_array( $type, FILTER_SANITIZE_SPECIAL_CHARS );

		if ( ! isset( $request[ $name ] ) || empty( $request[ $name ] ) ) {
			return $default;
		}

		return self::sanitize( $request[ $name ], $sanitize );
	}

	/**
	 * Sanitize value from methods post
	 *
	 * @since 1.0
	 * @param String $name
	 * @param Mixed $default
	 * @param String|Array $sanitize
	 * @return Mixed
	*/
	public static function post( $name, $default = '', $sanitize = 'rm_tags' )
	{
		return self::request( INPUT_POST, $name, $default, $sanitize );
	}

	/**
	 * Sanitize value from methods get
	 *
	 * @since 1.0
	 * @param String $name
	 * @param Mixed $default
	 * @param String|Array $sanitize
	 * @return Mixed
	*/
	public static function get( $name, $default = '', $sanitize = 'rm_tags' )
	{
		return self::request( INPUT_GET, $name, $default, $sanitize );
	}

	/**
	 * Sanitize value from cookie
	 *
	 * @since 1.0
	 * @param String $name
	 * @param Mixed $default
	 * @param String|Array $sanitize
	 * @return Mixed
	*/
	public static function cookie( $name, $default = '', $sanitize = 'rm_tags' )
	{
		return self::request( INPUT_COOKIE, $name, $default, $sanitize );
	}

	/**
	 * Get filtered super global server by key
	 *
	 * @since 1.0
	 * @param String $key
	 * @return String
	*/
	public static function server( $key )
	{
		$value = self::get_value_by( $_SERVER, strtoupper( $key ) );

		return self::rm_tags( $value, true );
	}

	/**
	 * Verify request by nonce
	 *
	 * @since 1.0
	 * @param String $name
	 * @param String $action
	 * @return Boolean
	*/
	public static function verify_nonce_post( $name, $action )
	{
		return wp_verify_nonce( self::post( $name, false ), $action );
	}

	/**
	 * Sanitize requests
	 *
	 * @since 1.0
	 * @param String $value
	 * @param String|Array $sanitize
	 * @return String
	*/
	public static function sanitize( $value, $sanitize )
	{
		if ( ! is_callable( $sanitize ) ) {
	    	return ( false === $sanitize ) ? $value : self::rm_tags( $value );
		}

		if ( is_array( $value ) ) {
			return array_map( $sanitize, $value );
		}

		return call_user_func( $sanitize, $value );
	}

	/**
	 * Properly strip all HTML tags including script and style
	 *
	 * @since 1.0
	 * @param Mixed String|Array $value
	 * @param Boolean $remove_breaks
	 * @return Mixed String|Array
	 */
	public static function rm_tags( $value, $remove_breaks = false )
	{
		if ( empty( $value ) || is_object( $value ) ) {
			return $value;
		}

		if ( is_array( $value ) ) {
			return array_map( __METHOD__, $value );
		}

	    return wp_strip_all_tags( $value, $remove_breaks );
	}

	/**
	 * Find the position of the first occurrence of a substring in a string
	 *
	 * @since 1.0
	 * @param String $value
	 * @param String $search
	 * @return Boolean
	*/
	public static function indexof( $value, $search )
	{
		return ( false !== strpos( $value, $search ) );
	}

	/**
	 * Verify request ajax
	 *
	 * @since 1.0
	 * @param null
	 * @return Boolean
	*/
	public static function is_request_ajax()
	{
		return ( strtolower( self::server( 'HTTP_X_REQUESTED_WITH' ) ) === 'xmlhttprequest' );
	}

	/**
	 * Get charset option
	 *
	 * @since 1.0
	 * @param Null
	 * @return String
	 */
	public static function get_charset()
	{
		return self::rm_tags( get_bloginfo( 'charset' ) );
	}

	/**
	 * Descode html entityes
	 *
	 * @since 1.0
	 * @param String $string
	 * @return String
	 */
	public static function html_decode( $string )
	{
		return html_entity_decode( $string, ENT_NOQUOTES, self::get_charset() );
	}

	/**
	 * Get value by array index
	 *
	 * @since 1.0
	 * @param Array $args
	 * @param String|int $index
	 * @return String
	 */
	public static function get_value_by( $args, $index, $default = '' )
	{
		if ( ! array_key_exists( $index, $args ) || empty( $args[ $index ] ) ) {
			return $default;
		}

		return $args[ $index ];
	}

	/**
	 * Admin sanitize url
	 *
	 * @since 1.0
	 * @param String $path
	 * @return String
	 */
	public static function get_admin_url( $path = '' )
	{
		return esc_url( get_admin_url( null, $path ) );
	}

	/**
	 * Site URL
	 *
	 * @since 1.0
	 * @param String $path
	 * @return String
	 */
	public static function get_site_url( $path = '' )
	{
		return esc_url( get_site_url( null, $path ) );
	}

	/**
	 * Permalink url sanitized
	 *
	 * @since 1.0
	 * @param Integer $post_id
	 * @return String
	 */
	public static function get_permalink( $post_id = 0 )
	{
		return esc_url( get_permalink( $post_id ) );
	}

	/**
	 * Add prefix in string
	 *
	 * @since 1.0
	 * @param String $after
	 * @param String $before
	 * @return String
	 */
	public static function add_prefix( $after, $before = '' ) {
		return $before . Core::PREFIX . $after;
	}

	/**
	 * Format and validate phone number with DDD
	 *
	 * @since 1.0
	 * @param String $phone
	 * @return String
	 */
	public static function format_phone_number( $phone )
	{
		$phone = preg_replace( array( '/[^\d]+/', '/^(?![1-9])0/' ), '', $phone );

		if ( strlen( $phone ) < 10 ) {
			return '';
		}

		return array( substr( $phone, 0, 2 ), substr( $phone, 2 ) );
	}

	/**
	 * Get date formatted for SQL
	 *
	 * @param String $date
	 * @param String $format
	 * @return String
	 */
	public static function convert_date_for_sql( $date, $format = 'Y-m-d' )
	{
		return empty( $date ) ? '' : self::convert_date( $date, $format, '/', '-' );
	}

	/**
	 * Conversion of date
	 *
	 * @param String $date
	 * @param String $format
	 * @param String $search
	 * @param String $replace
	 * @return String
	 */
	public static function convert_date( $date, $format = 'Y-m-d', $search = '/', $replace = '-' )
	{
		if ( $search && $replace ) {
			$date = str_replace( $search, $replace, $date );
		}

		return date_i18n( $format, strtotime( $date ) );
	}
	/**
	 * Conversion of date PT_BR
	 *
	 * @param String $date
	 * @param String $format
	 * @param String $search
	 * @param String $replace
	 * @return String
	 */
	public static function date_pt_format( $date )
	{
		return date( 'd/m/Y', strtotime( $date ) );
	}

	public static function get_template( $file, $args = array() )
	{
		if ( $args && is_array( $args ) ) {
			extract( $args );
		}

		$locale = Core::plugin_dir_path() . $file . '.php';

		if ( ! file_exists( $locale ) ) {
			return;
		}

		include $locale;
	}

	public static function str_to_float( $string )
	{
		return floatval( str_replace( ',', '.', $string ) );
	}
}

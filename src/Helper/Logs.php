<?php
namespace TH\Helper;

if ( ! function_exists( 'add_action' ) ) {
	exit( 0 );
}

use TH\Core;
use WC_Logger;

class Logs
{
    public static function ac_log( $title, $var )
	{
		$log          = new WC_Logger();
		$get_log_name = get_option( '_ac_log_file_name' );
		$log_name     = strtolower( get_bloginfo( 'name' ) );
		
		if ( $get_log_name ) {
			$log_name = get_option( '_ac_log_file_name' );
		}
		
		$log->add( $log_name.'-meliuz-log-', "{$title} : ".print_r( $var, true ) );
	}
}
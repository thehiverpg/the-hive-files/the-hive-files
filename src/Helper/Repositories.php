<?php
namespace TH\Helper;

if ( ! function_exists( 'add_action' ) ) {
	exit( 0 );
}

use TH\Core;

class Repositories
{
    public static function get_advantages_json()
	{
        $results    = array();
        $json       = file_get_contents( TH_MODULE_BASIC . 'advantages.json' );
        $args       = json_decode( $json, true );
        $advantages = $args['advantages'];

        foreach ( $advantages as $advantage ) {
            $results[0] = 'selecione';
            $results[$advantage['slug']] = $advantage['name'];
        }

        return $results;
    }
}

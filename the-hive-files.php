<?php
/*
 * Plugin Name: The Hive Files
 * Plugin URI:  https://thehiveteam.com
 * Version:     1.0.0
 * Author:      The Hive Team
 * Author URI:  https://thehiveteam.com
 * Text Domain: the-hive-files
 * Domain Path: /languages
 * License:     GPLv3 or later
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * Description: Construido para gerenciar as fichas de RPG do modulo GURPS.
 */

if ( ! function_exists( 'add_action' ) ) {
	exit( 0 );
}

require_once dirname( __FILE__ ) . '/constants.php';

function th_render_admin_notice_html( $message, $type = 'error' )
{
?>
	<div class="<?php echo $type; ?> notice is-dismissible">
		<p>
			<strong><?php _e( 'The Hive Files', TH_TEXTDOMAIN ); ?>: </strong>

			<?php echo $message; ?>
		</p>
	</div>
<?php
}


if ( version_compare( PHP_VERSION, '7.0', '<' ) ) {
	function th_admin_notice_php_version()
	{
		th_render_admin_notice_html(
			__( 'Sua versão no PHP não é suportada. Requerido >= 7.0', TH_TEXTDOMAIN )
		);
	}

	_th_load_notice( 'admin_notice_php_version' );
	return;
}

function _th_load_notice( $name )
{
	add_action( 'admin_notices', "th_{$name}" );
}

function _th_load_instances()
{
	require_once __DIR__ . '/vendor/autoload.php';
    
    TH\Core::instance();
    Carbon_Fields\Carbon_Fields::boot();

	do_action( 'th_init' );
}

function th_plugins_loaded_check()
{
	return _th_load_instances();
}

add_action( 'plugins_loaded', 'th_plugins_loaded_check', 0 );

function th_on_activation() 
{
	add_option( TH_OPTION_ACTIVATE, true );

	register_uninstall_hook( __FILE__, 'th_on_uninstall' );
}

function th_on_deactivation() {}

function th_on_uninstall() {}

register_activation_hook( __FILE__, 'th_on_activation' );
register_deactivation_hook( __FILE__, 'th_on_deactivation' );
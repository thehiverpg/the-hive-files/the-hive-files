<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit( 0 );
}

function th_define( $name, $value )
{
	if ( ! defined( $name ) ) {
		define( $name, $value );
	}
}

th_define( 'TH_SLUG', 'the-hive-files' );
th_define( 'TH_PREFIX', 'the-hive-files' );
th_define( 'TH_TEXTDOMAIN', 'the-hive-files' );
th_define( 'TH_VERSION', '1.0.0' );
th_define( 'TH_ROOT_PATH', dirname( __FILE__ ) . '/' );
th_define( 'TH_ROOT_SRC', TH_ROOT_PATH . 'src/' );
th_define( 'TH_MODULE_BASIC', TH_ROOT_SRC . 'ModuleBasic/' );
th_define( 'TH_ROOT_FILE', TH_ROOT_PATH . TH_SLUG . '.php' );

th_define( 'TH_OPTION_ACTIVATE', 'th_activate' );
